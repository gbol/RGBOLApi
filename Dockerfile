FROM ubuntu:18.04

RUN apt-get update && apt-get -y install gnupg2 curl software-properties-common \
	wget git default-jdk build-essential gradle

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
	apt-add-repository https://packages.microsoft.com/ubuntu/18.04/prod && \
	apt-get update 

RUN wget https://mran.blob.core.windows.net/install/mro/3.5.3/ubuntu/microsoft-r-open-3.5.3.tar.gz

RUN tar -xf microsoft-r-open-3.5.3.tar.gz && cd microsoft-r-open/ && ./install.sh

RUN apt-get install -y gdebi-core

RUN wget https://download2.rstudio.org/server/trusty/amd64/rstudio-server-1.2.1335-amd64.deb

RUN gdebi rstudio-server-1.2.1335-amd64.deb -n

RUN R CMD javareconf

RUN Rscript -e 'install.packages("rJava")' && \
	Rscript -e 'library("rJava")'

RUN echo "random"

RUN git clone https://gitlab.com/gbol/RGBOLApi

RUN wget http://download.systemsbiology.nl/gbol/life.gbol.GBOLApi.jar && \
	mv life.gbol.GBOLApi.jar RGBOLApi/inst/java/

RUN cd RGBOLApi && Rscript install.R 

RUN (adduser --disabled-password --gecos "" guest && echo "guest:guest"|chpasswd)

WORKDIR /app
COPY 'run-wait' .
EXPOSE 8787
CMD [ "./run-wait" ]