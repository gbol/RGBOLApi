#### See http://gbol.life/ for the complete documentation

### Docker is available

`docker run -it --rm -v $(pwd):/home/guest/work -p 49000:8787 wurssb/rgbol`

This will start a RStudio-server under port 49000 in which the RGBOLApi is up and running. Use the example code to generate a test RDF file.

#### Code example

```
library(RGBOLApi)
domain <- Domain$new("")

mainDnaObject <- domain$life.gbol$createNASequence("http://example.com/mainDnaObject")
aGene <- domain$life.gbol$createGene("http://example.com/oneGene")
location <- domain$life.gbol$createRegion("http://example.com/location1")
start <- domain$life.gbol$createExactPosition("http://example.com/posstart")
start$setPosition(10)
end <- domain$life.gbol$createExactPosition("http://example.com/posend")
end$setPosition(200)
location$setBegin(start)
location$setEnd(end)
location$setStrand(life.gbol.StrandPosition$ForwardStrandPosition)
aGene$setLocation(location) 

domain$save("test_res.ttl")
domain$close()
```

#### Installation instructions for Ubuntu 18.10 LTS

#### Install oracle java 1.8 or higher
```
sudo apt-add-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
```
#### Install R and RJava
`sudo apt-get install r-base r-cran-rjava `

#### Run following command and make sure that oracle java is selected
`sudo update-alternatives --config java`

#### configure java in R
`sudo R CMD javareconf`

#### Due to some bug in ubuntu
#### In /usr/lib/R/etc/ldpaths: Do replace 
`: ${R_JAVA_LD_LIBRARY_PATH=${JAVA_HOME}/lib/amd64/server}`
#### by this one:
`: ${R_JAVA_LD_LIBRARY_PATH=${JAVA_HOME}/jre/lib/amd64/server}`

##### Test if is work by starting R and enter command
`library("rJava")`

#### Install gradle 3.4.1
```
sudo add-apt-repository ppa:cwchien/gradle
sudo apt-get update
sudo apt-get install gradle
```
#### run the install script
`./install.sh`

