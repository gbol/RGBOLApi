if(!exists("TypeMap")) source("./R/Domain.R")
life.gbol.SequenceSet <- setRefClass(
  "life.gbol.SequenceSet",
  contains = list("OWLThing"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.SequenceSet",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://gbol.life/0.1/SequenceSet",life.gbol.SequenceSet,TypeMap)