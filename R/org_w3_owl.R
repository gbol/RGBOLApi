C_org.w3.owl<- setRefClass(
  "org.w3.owl",
  field = list(
    domain = "ANY"
  ),
  methods = list(
    initialize = function(domainIn)
    {
      domain <<- domainIn
    }
    ,createThing = function(iri){
      org.w3.owl.Thing$new(domain,iri)
    }
  )
)