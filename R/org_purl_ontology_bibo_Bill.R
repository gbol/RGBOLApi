if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("org.purl.ontology.bibo.Legislation")) source("./R/org_purl_ontology_bibo_Legislation.R")
org.purl.ontology.bibo.Bill <- setRefClass(
  "org.purl.ontology.bibo.Bill",
  contains = list("org.purl.ontology.bibo.Legislation"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","org.purl.ontology.bibo.domain.Bill",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://purl.org/ontology/bibo/Bill",org.purl.ontology.bibo.Bill,TypeMap)