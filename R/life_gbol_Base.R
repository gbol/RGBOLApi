if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.Location")) source("./R/life_gbol_Location.R")
life.gbol.Base <- setRefClass(
  "life.gbol.Base",
  contains = list("life.gbol.Location"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.Base",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getAt = function()
    {
      domain$getObj(.jcall(obj,"Llife/gbol/domain/Position;","getAt"))
    }
    ,setAt = function(val)
    {
      .jcall(obj,"V","setAt",.jcast(val$obj,"life.gbol.domain.Position"))
    }
    ,getStrand = function()
    {
      .jcall(obj,"Llife/gbol/domain/StrandPosition;","getStrand")
    }
    ,setStrand = function(val)
    {
      .jcall(obj,"V","setStrand",val)
    }
  )
)
assign("http://gbol.life/0.1/Base",life.gbol.Base,TypeMap)