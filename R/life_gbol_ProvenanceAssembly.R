if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.ProvenanceApplication")) source("./R/life_gbol_ProvenanceApplication.R")
life.gbol.ProvenanceAssembly <- setRefClass(
  "life.gbol.ProvenanceAssembly",
  contains = list("life.gbol.ProvenanceApplication"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.ProvenanceAssembly",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://gbol.life/0.1/ProvenanceAssembly",life.gbol.ProvenanceAssembly,TypeMap)