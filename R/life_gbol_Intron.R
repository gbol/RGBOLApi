if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.TranscriptionElement")) source("./R/life_gbol_TranscriptionElement.R")
life.gbol.Intron <- setRefClass(
  "life.gbol.Intron",
  contains = list("life.gbol.TranscriptionElement"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.Intron",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://gbol.life/0.1/Intron",life.gbol.Intron,TypeMap)