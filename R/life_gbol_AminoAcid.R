life.gbol.AminoAcid <- NA
load_life.gbol.AminoAcid <- function()
{
  toSet <- data.frame(matrix(NA, nrow=1, ncol=57))
  names(toSet) <- c("Asn","Hyl","Asp","Thr","AminoAcidOther","Pyl","Asx","Lys","Xle","Orn","Phe","Abu","Ile","bAla","Val","Leu","Ahe","Apm","MeGly","Ide","Gln","EtAsn","ThreeHyp","Des","His","Acp","Tyr","Xaa","Nva","Ala","TERM","MeIle","Cys","MeVal","Aad","Glu","Trp","EtGly","Pro","aHyl","Dbu","Aib","MeLys","Gly","Glx","Ser","Dpm","Met","Nle","Dpr","bAad","FourAbu","aIle","Sec","FourHyp","Arg","bAib")
  toSet$Asn <- J("life.gbol.domain.AminoAcid")$Asn
  toSet$Hyl <- J("life.gbol.domain.AminoAcid")$Hyl
  toSet$Asp <- J("life.gbol.domain.AminoAcid")$Asp
  toSet$Thr <- J("life.gbol.domain.AminoAcid")$Thr
  toSet$AminoAcidOther <- J("life.gbol.domain.AminoAcid")$AminoAcidOther
  toSet$Pyl <- J("life.gbol.domain.AminoAcid")$Pyl
  toSet$Asx <- J("life.gbol.domain.AminoAcid")$Asx
  toSet$Lys <- J("life.gbol.domain.AminoAcid")$Lys
  toSet$Xle <- J("life.gbol.domain.AminoAcid")$Xle
  toSet$Orn <- J("life.gbol.domain.AminoAcid")$Orn
  toSet$Phe <- J("life.gbol.domain.AminoAcid")$Phe
  toSet$Abu <- J("life.gbol.domain.AminoAcid")$Abu
  toSet$Ile <- J("life.gbol.domain.AminoAcid")$Ile
  toSet$bAla <- J("life.gbol.domain.AminoAcid")$bAla
  toSet$Val <- J("life.gbol.domain.AminoAcid")$Val
  toSet$Leu <- J("life.gbol.domain.AminoAcid")$Leu
  toSet$Ahe <- J("life.gbol.domain.AminoAcid")$Ahe
  toSet$Apm <- J("life.gbol.domain.AminoAcid")$Apm
  toSet$MeGly <- J("life.gbol.domain.AminoAcid")$MeGly
  toSet$Ide <- J("life.gbol.domain.AminoAcid")$Ide
  toSet$Gln <- J("life.gbol.domain.AminoAcid")$Gln
  toSet$EtAsn <- J("life.gbol.domain.AminoAcid")$EtAsn
  toSet$ThreeHyp <- J("life.gbol.domain.AminoAcid")$ThreeHyp
  toSet$Des <- J("life.gbol.domain.AminoAcid")$Des
  toSet$His <- J("life.gbol.domain.AminoAcid")$His
  toSet$Acp <- J("life.gbol.domain.AminoAcid")$Acp
  toSet$Tyr <- J("life.gbol.domain.AminoAcid")$Tyr
  toSet$Xaa <- J("life.gbol.domain.AminoAcid")$Xaa
  toSet$Nva <- J("life.gbol.domain.AminoAcid")$Nva
  toSet$Ala <- J("life.gbol.domain.AminoAcid")$Ala
  toSet$TERM <- J("life.gbol.domain.AminoAcid")$TERM
  toSet$MeIle <- J("life.gbol.domain.AminoAcid")$MeIle
  toSet$Cys <- J("life.gbol.domain.AminoAcid")$Cys
  toSet$MeVal <- J("life.gbol.domain.AminoAcid")$MeVal
  toSet$Aad <- J("life.gbol.domain.AminoAcid")$Aad
  toSet$Glu <- J("life.gbol.domain.AminoAcid")$Glu
  toSet$Trp <- J("life.gbol.domain.AminoAcid")$Trp
  toSet$EtGly <- J("life.gbol.domain.AminoAcid")$EtGly
  toSet$Pro <- J("life.gbol.domain.AminoAcid")$Pro
  toSet$aHyl <- J("life.gbol.domain.AminoAcid")$aHyl
  toSet$Dbu <- J("life.gbol.domain.AminoAcid")$Dbu
  toSet$Aib <- J("life.gbol.domain.AminoAcid")$Aib
  toSet$MeLys <- J("life.gbol.domain.AminoAcid")$MeLys
  toSet$Gly <- J("life.gbol.domain.AminoAcid")$Gly
  toSet$Glx <- J("life.gbol.domain.AminoAcid")$Glx
  toSet$Ser <- J("life.gbol.domain.AminoAcid")$Ser
  toSet$Dpm <- J("life.gbol.domain.AminoAcid")$Dpm
  toSet$Met <- J("life.gbol.domain.AminoAcid")$Met
  toSet$Nle <- J("life.gbol.domain.AminoAcid")$Nle
  toSet$Dpr <- J("life.gbol.domain.AminoAcid")$Dpr
  toSet$bAad <- J("life.gbol.domain.AminoAcid")$bAad
  toSet$FourAbu <- J("life.gbol.domain.AminoAcid")$FourAbu
  toSet$aIle <- J("life.gbol.domain.AminoAcid")$aIle
  toSet$Sec <- J("life.gbol.domain.AminoAcid")$Sec
  toSet$FourHyp <- J("life.gbol.domain.AminoAcid")$FourHyp
  toSet$Arg <- J("life.gbol.domain.AminoAcid")$Arg
  toSet$bAib <- J("life.gbol.domain.AminoAcid")$bAib
  unlockBinding("life.gbol.AminoAcid",as.environment("package:RGBOLApi"))
  assign("life.gbol.AminoAcid",toSet,as.environment("package:RGBOLApi"))
  lockBinding("life.gbol.AminoAcid",as.environment("package:RGBOLApi"))
}