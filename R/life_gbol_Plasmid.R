if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.CompleteNASequence")) source("./R/life_gbol_CompleteNASequence.R")
life.gbol.Plasmid <- setRefClass(
  "life.gbol.Plasmid",
  contains = list("life.gbol.CompleteNASequence"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.Plasmid",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getPlasmidName = function()
    {
      .jcall(obj,"Ljava/lang/String;","getPlasmidName")
    }
    ,setPlasmidName = function(val)
    {
      .jcall(obj,"V","setPlasmidName",val)
    }
  )
)
assign("http://gbol.life/0.1/Plasmid",life.gbol.Plasmid,TypeMap)