if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.BiologicalRecognizedRegion")) source("./R/life_gbol_BiologicalRecognizedRegion.R")
life.gbol.TransferOrigin <- setRefClass(
  "life.gbol.TransferOrigin",
  contains = list("life.gbol.BiologicalRecognizedRegion"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.TransferOrigin",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://gbol.life/0.1/TransferOrigin",life.gbol.TransferOrigin,TypeMap)