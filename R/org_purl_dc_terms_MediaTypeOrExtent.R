if(!exists("TypeMap")) source("./R/Domain.R")
org.purl.dc.terms.MediaTypeOrExtent <- setRefClass(
  "org.purl.dc.terms.MediaTypeOrExtent",
  contains = list("OWLThing"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","org.purl.dc.terms.domain.MediaTypeOrExtent",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getValue = function()
    {
      .jcall(obj,"Ljava/lang/String;","getValue")
    }
    ,setValue = function(val)
    {
      .jcall(obj,"V","setValue",val)
    }
    ,getLabel = function()
    {
      .jcall(obj,"Ljava/lang/String;","getLabel")
    }
    ,setLabel = function(val)
    {
      .jcall(obj,"V","setLabel",val)
    }
  )
)
assign("http://purl.org/dc/terms/MediaTypeOrExtent",org.purl.dc.terms.MediaTypeOrExtent,TypeMap)