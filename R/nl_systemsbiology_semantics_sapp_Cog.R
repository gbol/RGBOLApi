if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("nl.systemsbiology.semantics.sapp.Blast")) source("./R/nl_systemsbiology_semantics_sapp_Blast.R")
nl.systemsbiology.semantics.sapp.Cog <- setRefClass(
  "nl.systemsbiology.semantics.sapp.Cog",
  contains = list("nl.systemsbiology.semantics.sapp.Blast"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","nl.systemsbiology.semantics.sapp.domain.Cog",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getLetter = function()
    {
      .jcall(obj,"Ljava/lang/String;","getLetter")
    }
    ,setLetter = function(val)
    {
      .jcall(obj,"V","setLetter",val)
    }
    ,getMembershipClass = function()
    {
      .jcall(obj,"Ljava/lang/String;","getMembershipClass")
    }
    ,setMembershipClass = function(val)
    {
      .jcall(obj,"V","setMembershipClass",val)
    }
    ,getLabel = function()
    {
      .jcall(obj,"Ljava/lang/String;","getLabel")
    }
    ,setLabel = function(val)
    {
      .jcall(obj,"V","setLabel",val)
    }
    ,getOrganismName = function()
    {
      .jcall(obj,"Ljava/lang/String;","getOrganismName")
    }
    ,setOrganismName = function(val)
    {
      .jcall(obj,"V","setOrganismName",val)
    }
  )
)
assign("http://semantics.systemsbiology.nl/sapp/0.1/Cog",nl.systemsbiology.semantics.sapp.Cog,TypeMap)