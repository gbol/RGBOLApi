if(!exists("TypeMap")) source("./R/Domain.R")
life.gbol.Library <- setRefClass(
  "life.gbol.Library",
  contains = list("OWLThing"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.Library",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getAccpetedSameBarcodeRatio = function()
    {
      as.numeric(.jcall(obj,"Ljava/lang/Float;","getAccpetedSameBarcodeRatio"))
    }
    ,setAccpetedSameBarcodeRatio = function(val)
    {
      .jcall(obj,"V","setAccpetedSameBarcodeRatio",.jnew("java.lang.Float",.jfloat(val)))
    }
    ,remSample = function(val)
    {
      .jcall(obj,"V","remSample",.jcast(val$obj,"life.gbol.domain.Sample"))
    }
    ,addSample = function(val)
    {
      .jcall(obj,"V","addSample",.jcast(val$obj,"life.gbol.domain.Sample"))
    }
    ,getAllSample = function(val)
    {
      theList <- .jcall(obj,"Ljava/util/List;","getAllSample")
      walker <- .jcall(theList,"Ljava/util/Iterator;","iterator")
      toRet <- c()
      while(.jcall(walker,"Z","hasNext"))
      {
        toRet <- c(toRet,domain$getObj(.jcall(walker,"Ljava/lang/Object;","next")))
      }
      toRet
    }
    ,getLibraryNum = function()
    {
      decodeInt(.jcall(obj,"Ljava/lang/Integer;","getLibraryNum"))
    }
    ,setLibraryNum = function(val)
    {
      .jcall(obj,"V","setLibraryNum",.jnew("java.lang.Integer",as.integer(val)))
    }
    ,getBarcodeHitsAccepted = function()
    {
      decodeInt(.jcall(obj,"Ljava/lang/Integer;","getBarcodeHitsAccepted"))
    }
    ,setBarcodeHitsAccepted = function(val)
    {
      .jcall(obj,"V","setBarcodeHitsAccepted",.jnew("java.lang.Integer",as.integer(val)))
    }
    ,getFBarcodeFile = function()
    {
      .jcall(obj,"Ljava/lang/String;","getFBarcodeFile")
    }
    ,setFBarcodeFile = function(val)
    {
      .jcall(obj,"V","setFBarcodeFile",val)
    }
    ,getBarcodeHitsAcceptedRatio = function()
    {
      as.numeric(.jcall(obj,"Ljava/lang/Float;","getBarcodeHitsAcceptedRatio"))
    }
    ,setBarcodeHitsAcceptedRatio = function(val)
    {
      .jcall(obj,"V","setBarcodeHitsAcceptedRatio",.jnew("java.lang.Float",.jfloat(val)))
    }
    ,getPrimerHitsAcceptedRatio = function()
    {
      as.numeric(.jcall(obj,"Ljava/lang/Float;","getPrimerHitsAcceptedRatio"))
    }
    ,setPrimerHitsAcceptedRatio = function(val)
    {
      .jcall(obj,"V","setPrimerHitsAcceptedRatio",.jnew("java.lang.Float",.jfloat(val)))
    }
    ,getProvenance = function()
    {
      domain$getObj(.jcall(obj,"Llife/gbol/domain/Provenance;","getProvenance"))
    }
    ,setProvenance = function(val)
    {
      .jcall(obj,"V","setProvenance",.jcast(val$obj,"life.gbol.domain.Provenance"))
    }
    ,getRBarcodeFile = function()
    {
      .jcall(obj,"Ljava/lang/String;","getRBarcodeFile")
    }
    ,setRBarcodeFile = function(val)
    {
      .jcall(obj,"V","setRBarcodeFile",val)
    }
    ,getFBarcodeLength = function()
    {
      decodeInt(.jcall(obj,"Ljava/lang/Integer;","getFBarcodeLength"))
    }
    ,setFBarcodeLength = function(val)
    {
      .jcall(obj,"V","setFBarcodeLength",.jnew("java.lang.Integer",as.integer(val)))
    }
    ,getRFile = function()
    {
      .jcall(obj,"Ljava/lang/String;","getRFile")
    }
    ,setRFile = function(val)
    {
      .jcall(obj,"V","setRFile",val)
    }
    ,getTotalReads = function()
    {
      decodeInt(.jcall(obj,"Ljava/lang/Integer;","getTotalReads"))
    }
    ,setTotalReads = function(val)
    {
      .jcall(obj,"V","setTotalReads",.jnew("java.lang.Integer",as.integer(val)))
    }
    ,getPrimerHitsAccepted = function()
    {
      decodeInt(.jcall(obj,"Ljava/lang/Integer;","getPrimerHitsAccepted"))
    }
    ,setPrimerHitsAccepted = function(val)
    {
      .jcall(obj,"V","setPrimerHitsAccepted",.jnew("java.lang.Integer",as.integer(val)))
    }
    ,getFFile = function()
    {
      .jcall(obj,"Ljava/lang/String;","getFFile")
    }
    ,setFFile = function(val)
    {
      .jcall(obj,"V","setFFile",val)
    }
    ,getRBarcodeLength = function()
    {
      decodeInt(.jcall(obj,"Ljava/lang/Integer;","getRBarcodeLength"))
    }
    ,setRBarcodeLength = function(val)
    {
      .jcall(obj,"V","setRBarcodeLength",.jnew("java.lang.Integer",as.integer(val)))
    }
  )
)
assign("http://gbol.life/0.1/Library",life.gbol.Library,TypeMap)