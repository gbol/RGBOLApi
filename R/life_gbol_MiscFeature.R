if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.NAFeature")) source("./R/life_gbol_NAFeature.R")
life.gbol.MiscFeature <- setRefClass(
  "life.gbol.MiscFeature",
  contains = list("life.gbol.NAFeature"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.MiscFeature",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://gbol.life/0.1/MiscFeature",life.gbol.MiscFeature,TypeMap)