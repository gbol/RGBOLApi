C_org.w3.rdfsyntaxns<- setRefClass(
  "org.w3.rdfsyntaxns",
  field = list(
    domain = "ANY"
  ),
  methods = list(
    initialize = function(domainIn)
    {
      domain <<- domainIn
    }
    ,createList = function(iri){
      org.w3.rdfsyntaxns.List$new(domain,iri)
    }
  )
)