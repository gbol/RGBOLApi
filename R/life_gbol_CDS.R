if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.TranscriptFeature")) source("./R/life_gbol_TranscriptFeature.R")
life.gbol.CDS <- setRefClass(
  "life.gbol.CDS",
  contains = list("life.gbol.TranscriptFeature"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.CDS",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getProteinId = function()
    {
      .jcall(obj,"Ljava/lang/String;","getProteinId")
    }
    ,setProteinId = function(val)
    {
      .jcall(obj,"V","setProteinId",val)
    }
    ,getRibosomalSlippage = function()
    {
      as.logical(.jcall(obj,"Ljava/lang/Boolean;","getRibosomalSlippage"))
    }
    ,setRibosomalSlippage = function(val)
    {
      .jcall(obj,"V","setRibosomalSlippage",.jnew("java.lang.Boolean",as.logic(val)))
    }
    ,getException = function()
    {
      .jcall(obj,"Ljava/lang/String;","getException")
    }
    ,setException = function(val)
    {
      .jcall(obj,"V","setException",val)
    }
    ,getProduct = function()
    {
      .jcall(obj,"Ljava/lang/String;","getProduct")
    }
    ,setProduct = function(val)
    {
      .jcall(obj,"V","setProduct",val)
    }
    ,getCodonStart = function()
    {
      decodeInt(.jcall(obj,"Ljava/lang/Integer;","getCodonStart"))
    }
    ,setCodonStart = function(val)
    {
      .jcall(obj,"V","setCodonStart",.jnew("java.lang.Integer",as.integer(val)))
    }
    ,getProtein = function()
    {
      domain$getObj(.jcall(obj,"Llife/gbol/domain/Protein;","getProtein"))
    }
    ,setProtein = function(val)
    {
      .jcall(obj,"V","setProtein",.jcast(val$obj,"life.gbol.domain.Protein"))
    }
    ,remTranslExcept = function(val)
    {
      .jcall(obj,"V","remTranslExcept",.jcast(val$obj,"life.gbol.domain.TranslExcept"))
    }
    ,addTranslExcept = function(val)
    {
      .jcall(obj,"V","addTranslExcept",.jcast(val$obj,"life.gbol.domain.TranslExcept"))
    }
    ,getAllTranslExcept = function(val)
    {
      theList <- .jcall(obj,"Ljava/util/List;","getAllTranslExcept")
      walker <- .jcall(theList,"Ljava/util/Iterator;","iterator")
      toRet <- c()
      while(.jcall(walker,"Z","hasNext"))
      {
        toRet <- c(toRet,domain$getObj(.jcall(walker,"Ljava/lang/Object;","next")))
      }
      toRet
    }
  )
)
assign("http://gbol.life/0.1/CDS",life.gbol.CDS,TypeMap)