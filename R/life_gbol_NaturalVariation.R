if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.VariationFeature")) source("./R/life_gbol_VariationFeature.R")
life.gbol.NaturalVariation <- setRefClass(
  "life.gbol.NaturalVariation",
  contains = list("life.gbol.VariationFeature"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.NaturalVariation",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getFrequency = function()
    {
      as.numeric(.jcall(obj,"Ljava/lang/Double;","getFrequency"))
    }
    ,setFrequency = function(val)
    {
      .jcall(obj,"V","setFrequency",.jnew("java.lang.Double",as.numeric(val)))
    }
  )
)
assign("http://gbol.life/0.1/NaturalVariation",life.gbol.NaturalVariation,TypeMap)