if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.NASequence")) source("./R/life_gbol_NASequence.R")
life.gbol.PCRPrimer <- setRefClass(
  "life.gbol.PCRPrimer",
  contains = list("life.gbol.NASequence"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.PCRPrimer",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://gbol.life/0.1/PCRPrimer",life.gbol.PCRPrimer,TypeMap)