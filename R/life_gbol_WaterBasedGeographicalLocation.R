if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.GeographicalLocation")) source("./R/life_gbol_GeographicalLocation.R")
life.gbol.WaterBasedGeographicalLocation <- setRefClass(
  "life.gbol.WaterBasedGeographicalLocation",
  contains = list("life.gbol.GeographicalLocation"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.WaterBasedGeographicalLocation",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getOcean = function()
    {
      .jcall(obj,"Llife/gbol/domain/Ocean;","getOcean")
    }
    ,setOcean = function(val)
    {
      .jcall(obj,"V","setOcean",val)
    }
  )
)
assign("http://gbol.life/0.1/WaterBasedGeographicalLocation",life.gbol.WaterBasedGeographicalLocation,TypeMap)