if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("org.purl.ontology.bibo.Periodical")) source("./R/org_purl_ontology_bibo_Periodical.R")
org.purl.ontology.bibo.Magazine <- setRefClass(
  "org.purl.ontology.bibo.Magazine",
  contains = list("org.purl.ontology.bibo.Periodical"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","org.purl.ontology.bibo.domain.Magazine",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,remHasPart = function(val)
    {
      .jcall(obj,"V","remHasPart",.jcast(val$obj,"org.purl.ontology.bibo.domain.Issue"))
    }
    ,addHasPart = function(val)
    {
      .jcall(obj,"V","addHasPart",.jcast(val$obj,"org.purl.ontology.bibo.domain.Issue"))
    }
    ,getAllHasPart = function(val)
    {
      theList <- .jcall(obj,"Ljava/util/List;","getAllHasPart")
      walker <- .jcall(theList,"Ljava/util/Iterator;","iterator")
      toRet <- c()
      while(.jcall(walker,"Z","hasNext"))
      {
        toRet <- c(toRet,domain$getObj(.jcall(walker,"Ljava/lang/Object;","next")))
      }
      toRet
    }
  )
)
assign("http://purl.org/ontology/bibo/Magazine",org.purl.ontology.bibo.Magazine,TypeMap)