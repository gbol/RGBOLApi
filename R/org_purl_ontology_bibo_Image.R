if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("org.purl.ontology.bibo.Document")) source("./R/org_purl_ontology_bibo_Document.R")
org.purl.ontology.bibo.Image <- setRefClass(
  "org.purl.ontology.bibo.Image",
  contains = list("org.purl.ontology.bibo.Document"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","org.purl.ontology.bibo.domain.Image",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://purl.org/ontology/bibo/Image",org.purl.ontology.bibo.Image,TypeMap)