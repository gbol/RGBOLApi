if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("org.purl.ontology.bibo.Image")) source("./R/org_purl_ontology_bibo_Image.R")
org.purl.ontology.bibo.Map <- setRefClass(
  "org.purl.ontology.bibo.Map",
  contains = list("org.purl.ontology.bibo.Image"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","org.purl.ontology.bibo.domain.Map",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://purl.org/ontology/bibo/Map",org.purl.ontology.bibo.Map,TypeMap)