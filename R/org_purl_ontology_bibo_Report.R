if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("org.purl.ontology.bibo.Document")) source("./R/org_purl_ontology_bibo_Document.R")
org.purl.ontology.bibo.Report <- setRefClass(
  "org.purl.ontology.bibo.Report",
  contains = list("org.purl.ontology.bibo.Document"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","org.purl.ontology.bibo.domain.Report",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://purl.org/ontology/bibo/Report",org.purl.ontology.bibo.Report,TypeMap)