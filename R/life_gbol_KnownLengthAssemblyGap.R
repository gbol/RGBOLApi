if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.AssemblyGap")) source("./R/life_gbol_AssemblyGap.R")
life.gbol.KnownLengthAssemblyGap <- setRefClass(
  "life.gbol.KnownLengthAssemblyGap",
  contains = list("life.gbol.AssemblyGap"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.KnownLengthAssemblyGap",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://gbol.life/0.1/KnownLengthAssemblyGap",life.gbol.KnownLengthAssemblyGap,TypeMap)