if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("com.xmlns.foaf.Agent")) source("./R/com_xmlns_foaf_Agent.R")
com.xmlns.foaf.Organization <- setRefClass(
  "com.xmlns.foaf.Organization",
  contains = list("com.xmlns.foaf.Agent"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","com.xmlns.foaf.domain.Organization",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getLegalName = function()
    {
      .jcall(obj,"Ljava/lang/String;","getLegalName")
    }
    ,setLegalName = function(val)
    {
      .jcall(obj,"V","setLegalName",val)
    }
    ,getLogo = function()
    {
      .jcall(obj,"Ljava/lang/String;","getLogo")
    }
    ,setLogo = function(val)
    {
      .jcall(obj,"V","setLogo",val)
    }
    ,getPhone = function()
    {
      .jcall(obj,"Ljava/lang/String;","getPhone")
    }
    ,setPhone = function(val)
    {
      .jcall(obj,"V","setPhone",val)
    }
    ,getHomepage = function()
    {
      .jcall(obj,"Ljava/lang/String;","getHomepage")
    }
    ,setHomepage = function(val)
    {
      .jcall(obj,"V","setHomepage",val)
    }
    ,getMbox = function()
    {
      .jcall(obj,"Ljava/lang/String;","getMbox")
    }
    ,setMbox = function(val)
    {
      .jcall(obj,"V","setMbox",val)
    }
    ,getBased_near = function()
    {
      .jcall(obj,"Ljava/lang/String;","getBased_near")
    }
    ,setBased_near = function(val)
    {
      .jcall(obj,"V","setBased_near",val)
    }
  )
)
assign("http://xmlns.com/foaf/0.1/Organization",com.xmlns.foaf.Organization,TypeMap)