if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.GenomicFeature")) source("./R/life_gbol_GenomicFeature.R")
life.gbol.Homology <- setRefClass(
  "life.gbol.Homology",
  contains = list("life.gbol.GenomicFeature"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.Homology",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getHomologousTo = function()
    {
      .jcall(obj,"Ljava/lang/String;","getHomologousTo")
    }
    ,setHomologousTo = function(val)
    {
      .jcall(obj,"V","setHomologousTo",val)
    }
  )
)
assign("http://gbol.life/0.1/Homology",life.gbol.Homology,TypeMap)