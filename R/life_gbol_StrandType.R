life.gbol.StrandType <- NA
load_life.gbol.StrandType <- function()
{
  toSet <- data.frame(matrix(NA, nrow=1, ncol=5))
  names(toSet) <- c("SingleStrandedRNA","ComplementaryDNA","SingleStrandedDNA","DoubleStrandedDNA","DoubleStrandedRNA")
  toSet$SingleStrandedRNA <- J("life.gbol.domain.StrandType")$SingleStrandedRNA
  toSet$ComplementaryDNA <- J("life.gbol.domain.StrandType")$ComplementaryDNA
  toSet$SingleStrandedDNA <- J("life.gbol.domain.StrandType")$SingleStrandedDNA
  toSet$DoubleStrandedDNA <- J("life.gbol.domain.StrandType")$DoubleStrandedDNA
  toSet$DoubleStrandedRNA <- J("life.gbol.domain.StrandType")$DoubleStrandedRNA
  unlockBinding("life.gbol.StrandType",as.environment("package:RGBOLApi"))
  assign("life.gbol.StrandType",toSet,as.environment("package:RGBOLApi"))
  lockBinding("life.gbol.StrandType",as.environment("package:RGBOLApi"))
}