if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.ProvenanceMapping")) source("./R/life_gbol_ProvenanceMapping.R")
life.gbol.Bowtie2 <- setRefClass(
  "life.gbol.Bowtie2",
  contains = list("life.gbol.ProvenanceMapping"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.Bowtie2",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,getSQ = function()
    {
      .jcall(obj,"Ljava/lang/String;","getSQ")
    }
    ,setSQ = function(val)
    {
      .jcall(obj,"V","setSQ",val)
    }
    ,getHD = function()
    {
      .jcall(obj,"Ljava/lang/String;","getHD")
    }
    ,setHD = function(val)
    {
      .jcall(obj,"V","setHD",val)
    }
    ,getCO = function()
    {
      .jcall(obj,"Ljava/lang/String;","getCO")
    }
    ,setCO = function(val)
    {
      .jcall(obj,"V","setCO",val)
    }
    ,getPG = function()
    {
      .jcall(obj,"Ljava/lang/String;","getPG")
    }
    ,setPG = function(val)
    {
      .jcall(obj,"V","setPG",val)
    }
  )
)
assign("http://gbol.life/0.1/Bowtie2",life.gbol.Bowtie2,TypeMap)