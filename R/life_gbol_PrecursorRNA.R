if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.Transcript")) source("./R/life_gbol_Transcript.R")
life.gbol.PrecursorRNA <- setRefClass(
  "life.gbol.PrecursorRNA",
  contains = list("life.gbol.Transcript"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.PrecursorRNA",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
  )
)
assign("http://gbol.life/0.1/PrecursorRNA",life.gbol.PrecursorRNA,TypeMap)