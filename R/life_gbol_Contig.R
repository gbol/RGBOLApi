if(!exists("TypeMap")) source("./R/Domain.R")
if(!exists("life.gbol.UncompleteNASequence")) source("./R/life_gbol_UncompleteNASequence.R")
life.gbol.Contig <- setRefClass(
  "life.gbol.Contig",
  contains = list("life.gbol.UncompleteNASequence"),
  methods = list(
    initialize = function(domainIn,iri)
    {
      obj <<- .jcall(domainIn$domain,"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;","make","life.gbol.domain.Contig",iri)
      domain <<- domainIn
      domainIn$addObj(.self)
    }
    ,remSourceReads = function(val)
    {
      .jcall(obj,"V","remSourceReads",.jcast(val$obj,"life.gbol.domain.Read"))
    }
    ,addSourceReads = function(val)
    {
      .jcall(obj,"V","addSourceReads",.jcast(val$obj,"life.gbol.domain.Read"))
    }
    ,getAllSourceReads = function(val)
    {
      theList <- .jcall(obj,"Ljava/util/List;","getAllSourceReads")
      walker <- .jcall(theList,"Ljava/util/Iterator;","iterator")
      toRet <- c()
      while(.jcall(walker,"Z","hasNext"))
      {
        toRet <- c(toRet,domain$getObj(.jcall(walker,"Ljava/lang/Object;","next")))
      }
      toRet
    }
  )
)
assign("http://gbol.life/0.1/Contig",life.gbol.Contig,TypeMap)