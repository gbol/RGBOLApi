#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C "$DIR" pull

# Pull latest
git clone https://gitlab.com/gbol/GBOLapi.git ../GBOLApi
../GBOLApi/install.sh
 
Rscript install.R
